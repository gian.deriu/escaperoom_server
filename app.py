d={}
try:
  with open("configurazione.txt", "r") as text_file:
      lines = text_file.readlines()
  for l in lines:
    d[l.split("=")[0].strip().upper()]=l.split("=")[1].strip()
except:
  pass
    
RASPBERRY          = d.get("RASPBERRY", "False") in ["True", "true", "TRUE"]
AUTOSTART          = d.get("AUTOSTART", "False") in ["True", "true", "TRUE"]

AUTOSTART_SECONDS  = int(d.get("AUTOSTART_SECONDS", 30))
INITIAL_GAME_TIME  = int(d.get("INITIAL_GAME_TIME", 60*60)) # [seconds] the initial game time

TIME_RUNNING_OUT   = int(d.get("TIME_RUNNING_OUT", 60)) # [seconds] the last seconds of the game. 
FINAL_THEME_LENGHT = int(d.get("FINAL_THEME_LENGHT", 20*60)) # [seconds]
AUDIO_PATH         = d.get("AUDIO_PATH", "/home/pi/Desktop/audio_pirati") 

STOP_CANCELLA      = d.get("STOP_CANCELLA", "False") in ["True", "true", "TRUE"]
STOP_AUDIO_BUTTON  =  d.get("STOP_AUDIO_BUTTON", "False") in ["True", "true", "TRUE"]
TEMPO_BONUS        = int(d.get("TEMPO_BONUS", 180)) # [seconds] the last seconds of the game. 
# raspberry GPIO pins

BB_pin     = d.get("BB_pin", 17)
START_pin  = d.get("START_pin", 27)
FINISH_pin = d.get("FINISH_pin", 22)

from multiprocessing import Process
import os
import random
import time
from flask import Flask, request, render_template, session, flash, redirect, url_for, jsonify

from celery import Celery
from celery.app.control import Inspect
import json
from redis import Redis
import datetime

if RASPBERRY:
  from gpiozero import Button
  
  def check_buttons():
    pass
  def spegni():
    os.system('killall celery')
    time.sleep(5)
    os.system('shutdown -P now')
else:
  class Button():
    def __init__ (self, pin, pull_up):
      self.is_pressed = pull_up
    
    def press (self):
      self.is_pressed = True
      
    def release (self):
      self.is_pressed = False
      
  def check_buttons():
        val =get_redis("val")
        if val=='1':
          start_input.press()
          bb_input.release()
        if val=='2':
          start_input.release()
          bb_input.press()
          finish_input.release()
        if val=='3':
          start_input.release()
          finish_input.press()
          bb_input.release()
        if val=='0':
          start_input.release()
          finish_input.release()
          bb_input.release()

  def spegni():
    exit(0)

app = Flask(__name__)


bb_input     = Button(BB_pin,     pull_up = False)
start_input  = Button(START_pin,  pull_up = False)
finish_input = Button(FINISH_pin, pull_up = False)

# Celery configuration
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'


# Initialize Celery
ce = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
ce.conf.update(app.config)

ce.control.purge()
redisc= Redis(host="localhost", port=6379, charset="utf-8", decode_responses=True)
    
redisc.flushdb()

print (" *** StanzEnigma ***")
print ("WELCOME")


def set_redis(key, val): 
    redisc= Redis(host="localhost", port=6379, charset="utf-8", decode_responses=True)
    
    redisc.set(key, val)


def get_redis(key):
    redisc= Redis(host="localhost", port=6379, charset="utf-8", decode_responses=True)

    if redisc.exists(key):
      return redisc.get(key)
    
    return None

def del_redis(key):
    redisc= Redis(host="localhost", port=6379, charset="utf-8", decode_responses=True)

    if redisc.exists(key):
      return redisc.delete(key)
    
    return None

def autostart():
    
    print ("Autostart in ...")
    for i in range(0, AUTOSTART_SECONDS):
      print ("{} second(s)".format(AUTOSTART_SECONDS - i))
      time.sleep(1)
    print ("autostarting")

    redisc= Redis(host="localhost", port=6379, charset="utf-8", decode_responses=True)
    task_id = None
    
    for k in redisc.scan_iter("*celery-task*"):
      
      
      a=get_redis(k)
      
      a=json.loads(a)
      
      task_id = a['task_id']

      print (task_id)
      
    if task_id:
      print ("already started")
      return None
    else:
      print ("starting")
      set_redis("total_time", INITIAL_GAME_TIME)
      task = pirates_timer.apply_async(queue="pirates")
      task_id = task.id
      time.sleep(10)


@ce.task(bind=True)
def pirates_timer(self):
    """Timer"""
    import pygame
    
    print ("Starting room...")

    pygame.mixer.init()
    
    current_audio = "audio_iniziale.ogg"
    
    while True:

      bb_started = False
      start_started = False
      finish_started = False
      hurry_up = False
      sec_final = 0
      seconds = 0
      reset_countdown = 5
      total_seconds = int(get_redis("total_time"))
      self.update_state(state='PROGRESS',
                        meta={'current': seconds, 'total': total_seconds,
                              'status': "Pronto!"})
      
      if start_input.is_pressed:
        print ("Rilascia il tasto!")
        time.sleep(5)
     
      check_buttons()
      
      pygame.mixer.music.fadeout(600)
      print("WELCOME")
      pygame.mixer.music.load(os.path.join(AUDIO_PATH, "welcome.wav"))
      pygame.mixer.music.play()#fade_ms = 400)
      del_redis("stop_pirates")
          
      
      while not start_input.is_pressed: 
        check_buttons()
        time.sleep(1)
          
      start_started = True
      #stop music
      pygame.mixer.music.fadeout(600)
      print("Starting theme song...")
      pygame.mixer.music.load(os.path.join(AUDIO_PATH, current_audio))
      pygame.mixer.music.play()#fade_ms = 400)

      
      while True:
        if not finish_started:
          self.update_state(state='PROGRESS',
                        meta={'current': seconds, 'total': total_seconds,
                              'status': "{}".format(str(datetime.timedelta(seconds=total_seconds-seconds)))})
      
        total_seconds = int(get_redis("total_time"))
        if get_redis("stop_pirates"):
          pygame.mixer.music.fadeout(600)
          del_redis("stop_pirates")
          return {'current': seconds, 'total': total_seconds, 'status': "Fermato a {}".format(str(datetime.timedelta(seconds=total_seconds-seconds)))}
          break
      
        if get_redis("inc_dec_time"):
          pygame.mixer.music.fadeout(600)
          s=pygame.mixer.Sound(os.path.join(AUDIO_PATH, "Bonus_Sound_Effects.wav"))
          l=s.get_length()
          s.play()
          time.sleep(l)
          s.stop()
          pygame.mixer.music.load(os.path.join(AUDIO_PATH, current_audio))
          start_sec = 60*60-(total_seconds-seconds)
          if start_sec<0:
            start_sec=0
          pygame.mixer.music.play(start=start_sec, fade_ms=1000)
          del_redis("inc_dec_time")
          
        if total_seconds - seconds < TIME_RUNNING_OUT and hurry_up==False:
            hurry_up = True
            #stop music
            pygame.mixer.music.fadeout(10)
           # pygame.mixer.music.stop()
            print("hurry_up")
            current_audio = "AUDIO-finale-di-sottofondo3.ogg"
            pygame.mixer.music.load(os.path.join(AUDIO_PATH, current_audio))
            pygame.mixer.music.play(fade_ms=100)
          
       
        
        if bb_input.is_pressed and not bb_started:
            bb_started = True
            #stop music
            pygame.mixer.music.fadeout(600)
            print("BB phase")
            current_audio = "Audio_balance_board.ogg"
            pygame.mixer.music.load(os.path.join(AUDIO_PATH, current_audio))
            pygame.mixer.music.play()#fade_ms = 400)

            
        
        
        if finish_input.is_pressed and not finish_started:
            finish_started = True
            #stop music
            pygame.mixer.music.fadeout(600)
            print("Final phase")
            self.update_state(state='PROGRESS',
                        meta={'current': seconds, 'total': total_seconds,
                              'status': "VITTORIA! in: {}".format(str(datetime.timedelta(seconds=seconds)))})
            current_audio = "audio_vittoria.ogg"
            pygame.mixer.music.load(os.path.join(AUDIO_PATH, current_audio))
            pygame.mixer.music.play()

            
        
        
        if finish_started:
          print ("{}".format(FINAL_THEME_LENGHT-sec_final))
          sec_final += 1
          if sec_final > FINAL_THEME_LENGHT:
            print ("Shutdown...")
            pygame.mixer.music.fadeout(600)
            spegni()
            break
        
        
        if start_input.is_pressed:
          if reset_countdown <=0:
            print ("Reset")
            set_redis("total_time", INITIAL_GAME_TIME)
          #  path_aiuti = os.path.join(AUDIO_PATH, "audioaiutopirati")
            pygame.mixer.music.load(os.path.join(AUDIO_PATH, "reset.wav"))
            pygame.mixer.music.play()#fade_ms = 400)
            break
          print ("Reset in {} second(s)".format(reset_countdown))
          reset_countdown -=1
        else:
          reset_countdown = 5
        
        check_buttons()
    
        
        
        
        
        
        time.sleep(1)
        seconds+=1
        if seconds >= total_seconds:
          if get_redis("timesup") == None:
            set_redis("timesup", 1)
          self.update_state(state='PROGRESS',
                        meta={'current': seconds, 'total': total_seconds,
                              'status': "TEMPO SCADUTO!"})
          seconds_fail=0
          while True:
            if get_redis("moretime"): 
              t = get_redis("total_time")
              if t:
                set_redis("total_time", int(t)+TEMPO_BONUS)
              del_redis("timesup")
              del_redis("moretime")
              pygame.mixer.music.fadeout(600)
              print("{} more seconds".format(TEMPO_BONUS))
              pygame.mixer.music.load(os.path.join(AUDIO_PATH, "audiopiu30.wav"))
              pygame.mixer.music.play()
              break
            seconds_fail+=1
            if seconds_fail> 60*10:
              print ("Shutdown...")
              pygame.mixer.music.fadeout(600)
              spegni()
            time.sleep(1)
          #return {'current': seconds, 'total': total_seconds, 'status': "Tempo scaduto!"}
    

@app.route('/', methods=['GET', 'POST'])
def index():


    path_aiuti = os.path.join(AUDIO_PATH, "audioaiutopirati")
    files = os.listdir(path_aiuti)

    wav = [f for f in files if os.path.isfile(os.path.join(path_aiuti, f)) and f.endswith(".wav")]

    print (wav)
    st1 = [f for f in wav if f.lower().startswith("st1")]
    st2 = [f for f in wav if f.lower().startswith("st2")]
    st3 = [f for f in wav if f.lower().startswith("st3")]
    others = [f for f in wav if not f.lower().startswith("st1") and not f.lower().startswith("st2") and not f.lower().startswith("st3")]
    
    if get_redis("timesup"):
      time_bonus_button = True
    else:    
      time_bonus_button = False
      

    return render_template('index.html', email=session.get('email', ''), stop_audio_button = STOP_AUDIO_BUTTON,
             time_bonus_button=time_bonus_button, stop_cancella = STOP_CANCELLA, autostart=AUTOSTART, st1 = st1, st2 = st2, st3 = st3, others = others)
   


@app.route('/startpirates', methods=['POST'])
def startpirates():
    redisc= Redis(host="localhost", port=6379, charset="utf-8", decode_responses=True)
    task_id = None
    
    for k in redisc.scan_iter("*celery-task*"):
      
      
      a=get_redis(k)
      
      a=json.loads(a)
      
      task_id = a['task_id']

      print (task_id)
      
    if task_id:
      #flash('Task already exists')
      return jsonify({}), 404, {'Location': 'None'}
    else:
      set_redis("total_time", INITIAL_GAME_TIME)
      task = pirates_timer.apply_async(queue="pirates")
      task_id = task.id
      return jsonify({}), 202, {'Location': url_for('taskstatus',
                                                  task_id=task_id)}

@app.route('/resumepirates', methods=['POST'])
def resumepirates():
    
    redisc= Redis(host="localhost", port=6379, charset="utf-8", decode_responses=True)
    task_id = None
    
    for k in redisc.scan_iter("*celery-task*"):
      
      
      a=get_redis(k)
      
      a=json.loads(a)
      
      task_id = a['task_id']

      print (task_id)
    if task_id:
      return jsonify({}), 202, {'Location': url_for('taskstatus', task_id=task_id)}
    else:
      print ("No tasks running")
      return jsonify({}), 210, {}
      
      

@app.route('/clearpirates', methods=['POST'])
def clearpirates():
    
    redisc= Redis(host="localhost", port=6379, charset="utf-8", decode_responses=True)
    task_id = None
    
    print ("Clearing...")
    for k in redisc.scan_iter("*celery-task*"):
      print (k)
      a=get_redis(k)
      
      a=json.loads(a)
      
      status = a['status']
      if status == "PROGRESS":
        print ("Still running")
        return jsonify({}), 404, {}
      del_redis(k)
      
    return jsonify({}), 202, {}
      
      
@app.route('/inctimer', methods=['POST'])
def inctimer():

    t = get_redis("total_time")
    if t:
    
      set_redis("total_time", int(t)+60*5)
      set_redis("inc_dec_time", 1)
   
      
    
    return jsonify({}), 202, {'Message': "incremented"}


@app.route('/dectimer', methods=['POST'])
def dectimer():

    t = get_redis("total_time")
    if t:
    
      set_redis("total_time", int(t)-60*5)
      set_redis("inc_dec_time", 1)
      
    
    return jsonify({}), 202, {'Message': "incremented"}


@app.route('/stoppirates', methods=['POST'])
def stoppirates():

    set_redis("stop_pirates", 1)
   
    return jsonify({}), 202, {'Message': "incremented"}

@app.route('/moretimepirates', methods=['POST'])
def moretimepirates():

    set_redis("moretime", 1)
   
    return jsonify({}), 202, {'Message': "moretime"}

@app.route('/playWAV', methods=['POST'])
def playWAV():
    import pygame
    pygame.mixer.init()
    wav = request.args.get('file')
    print ("playing: ")
    print (wav)

    
    path_aiuti = os.path.join(AUDIO_PATH, "audioaiutopirati")
    if wav=="stop":
      pygame.mixer.music.fadeout(400)
    else:
      pygame.mixer.music.load(os.path.join(path_aiuti, wav))
      pygame.mixer.music.play()#fade_ms = 400)
   
    return jsonify({}), 202, {'Message': "played"}


@app.route('/shutdownpirates', methods=['POST'])
def shutdownpirates():
    import pygame
    pygame.mixer.init()
    
    print ("A presto!")
    
    set_redis("stop_pirates", 1)

    pygame.mixer.music.load(os.path.join(AUDIO_PATH, "shutdown.wav"))
    pygame.mixer.music.play()#fade_ms = 400)
    
    time.sleep(3)
    
    spegni()
    
    return jsonify({}), 202, {'Message': "played"}



@app.route('/status/<task_id>')
def taskstatus(task_id):
    task = pirates_timer.AsyncResult(task_id)
    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'current': 0,
            'total': 1,
            'status': 'Caricamento...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'current': task.info.get('current', 0),
            'total': task.info.get('total', 1),
            'status': task.info.get('status', '')
        }
        if 'result' in task.info:
            response['result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'current': 1,
            'total': 1,
            'status': str(task.info),  # this is the exception raised
        }
    return jsonify(response)


if __name__ == '__main__':
  if AUTOSTART:
    p = Process(target=autostart)
    p.start()
    p.join()
  app.run(debug=False, host='0.0.0.0', port=80)

